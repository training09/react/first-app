interface EmpObjInterface {

    fName: string;
    lName: string;
    age: number;
}

export default EmpObjInterface;