import axios from "axios";
import { useEffect, useState } from "react";

const ApiComponent = () => {

    const [res, setRes] = useState([]);

    useEffect(()=> {
        axios.get("https://61f695832e1d7e0017fd6df8.mockapi.io/employees").then((res)=> {
            setRes(res.data);
        });
    }, []);

    return (
    <div>
        {res.map((data:any) => 
            <div key={data.id}>
                <label id="">{data.name}</label>
                <label id="">{data.age}</label>
                <label id="">{data.email}</label>
                <label id="">{data.mobile}</label>
                <label id="">{data.address}</label>
            </div>
        )}
    </div>
    );
}

export default ApiComponent;