import { BrowserRouter, Routes, Route, MemoryRouter } from "react-router-dom";
import FirstComponent from "./FirstComponent";
import SecondComponent from "./SecondComponent";
import DefaultComponent from "./DefaultComponent";
import ThirdComponent from "./ThirdComponent";
import FourthComponent from "./FourthComponent";
import { FullNameContext } from "../App";
import UseCntxtComp1 from "./UseCntxtComp1";
import UseCntxtComp3 from "./UseCntxtComp3";
import UseCntxtComp4 from "./UseCntxtComp4";
import LoginComponent from "./LoginComponent";
import ApiComponent from "./api/ApiComponent";
import PreStateComp from "./prestate/PreStateComp";

const RouterComponent = () => {

    return (
        <BrowserRouter>
        {/* <MemoryRouter> */}
            <Routes>
                <Route path="/" element={<LoginComponent />} />
                <Route path="/default" element={<DefaultComponent />} />
                <Route path="/first" element={<FirstComponent />} />
                <Route path="/second/:fName/:lName" element={<SecondComponent />} />
                <Route path="/third" element={<ThirdComponent />} />
                <Route path="/fourth" element={<FourthComponent />} />
                <Route path="/comp1" element={<UseCntxtComp1 />} />
                <Route path="/comp3" element={<UseCntxtComp3 />} />
                <Route path="/comp4" element={<UseCntxtComp4 />} />
                <Route path="/apicomp" element={<ApiComponent />} />
                <Route path="/prestate" element={<PreStateComp />} />
            </Routes>
            {/* </MemoryRouter> */}
        </BrowserRouter>
    );
}

export default RouterComponent;