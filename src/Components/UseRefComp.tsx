import { useRef } from "react";
import ReactDOM from "react-dom/client";

const UseRefComp = () => {

    const inputTxt = useRef<any>(0);

    const focusInput = () => {
        inputTxt.current.focus();
        //console.log("Input Txt Valeu=", inputTxt.current);
        console.log("Input Txt Valeu=", inputTxt.current.value);
    };

    return (
        <div>
          <input type="text" ref={inputTxt} />
          <button onClick={focusInput}>Focus Input</button>
        </div>
      );
}

export default UseRefComp;