//import { useContext } from 'react';
import React, {useContext}  from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { FullNameContext } from '../App';


const FourthComponent = () => {

  // const obj:any = useContext(UserContext);
  const fullNameCntxt:any = useContext(FullNameContext);
  
  //const params = useParams();

  const navigate = useNavigate();

  //const fName = params.fName;

  //const lName = params.lName;

  return (
    <div>FourthComponent
      <h2>First Name:{fullNameCntxt.fName}</h2><br />
      <h2>Last Name:{fullNameCntxt.lName}</h2><br />
    </div>
  )
}

export default FourthComponent;