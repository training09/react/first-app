import axios from "axios";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
// import { FullNameContext } from "../App";

const SecondComponent = () => {

    const params = useParams();

    const navigate = useNavigate();

    const fName = params.fName;

    const lName = params.lName;

    const [response, setResponse] = useState();

    const nextComp = () => {
        navigate(`/third`);
    }

    useEffect(()=> {
        console.log("Inside the SecondComponent........AXIOS API Call.........")
        axios.get("https://61f695832e1d7e0017fd6df8.mockapi.io/employees").then((res)=> {
            //console.log("res------------>",res);
            //console.log("res.data------------>",res.data);
            setResponse(res.data);
        });
    },[]);

    return (
        // <FullNameContext.Provider value={{ fName: fName, lName: lName }}>
            <div>
                <h1>Inside Second Component............</h1><br />
                <h2>First Name:{fName}</h2><br />
                <h2>Last Name:{lName}</h2><br />
                <button onClick={nextComp}>Next Comp</button><br />
                {/* {
                    response.forEach(ele => {
                       <div>{ele.id}</div>>
                        ele.name,
                        ele.age,
                        ele.email
                    })
                } */}
            </div>

        // </FullNameContext.Provider>
    );
}

export default SecondComponent;