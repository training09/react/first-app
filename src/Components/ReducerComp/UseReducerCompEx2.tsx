import { useReducer } from "react";

const UseReducerCompEx2 = () => {

    type EmpObjType = {
        fName: string;
        lName: string;
        age: number;
    }

    function reducerLogic(state:any, action:any) {
        switch(action.type) {
            case "increment":
                //return {fName: action.fName, age: action.age, lName: action.lName};
                state.fName = action.fName;
                state.age = action.age;
                state.lName = action.lName;
                console.log("stateValue=", state);
                return state;
            case "decrement":
                state.age = action.age;
                return state;
            case "reset":
                return {fName: "Elephant"};
            default:
                break;
        }
    }

    // const reducerLogic = (state:any, action:any) => {
    //     switch(action.type) {
    //         case "increment":
    //             return {count: state.count + action.value};
    //         case "decrement":
    //             return {count: state.count - action.value};
    //         case "reset":
    //             return {count: 0};
    //         default:
    //             break;
    //     }
    // }

    type ActionType = {
        type: string;
        fName: string;
        lName: string;
        age: number;
    }

    const empObj = {
        fName: "Tiger",
        lName: "Lion",
        age: "10"
    }

    const [state, dispatch] = useReducer(reducerLogic, empObj);

    const incrActionObj = {type: "increment", fName: "TigerR", lName:"Lion", age: 10};

    const decrActionObj = {type: "decrement", age: "4"};

    const resetActionObj = {type: "reset"};

    return (
        <div>
            <h1>Use Reducer</h1>
            {state?.fName}
            <br />
            {state?.lName}
            <br />
            {state?.age}
            <br />
            <br />
            &nbsp;&nbsp;<button onClick={()=>dispatch(incrActionObj)}>+</button>&nbsp;
            &nbsp;&nbsp;<button onClick={()=>dispatch(decrActionObj)}>-</button>&nbsp;
            &nbsp;&nbsp;<button onClick={()=>dispatch(resetActionObj)}>Reset</button>                        
        </div>
    );
}

export default UseReducerCompEx2;