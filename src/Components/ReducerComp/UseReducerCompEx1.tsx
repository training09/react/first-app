import { useReducer } from "react";

const UseReducerCompEx1 = () => {

    function reducerLogic(state:any, action:any) {
        switch(action.type) {
            case "increment":
                return {count: state.count + action.value};
            case "decrement":
                return {count: state.count - action.value};
            case "reset":
                return {count: 0};
            default:
                break;
        }
    }

    // const reducerLogic = (state:any, action:any) => {
    //     switch(action.type) {
    //         case "increment":
    //             return {count: state.count + action.value};
    //         case "decrement":
    //             return {count: state.count - action.value};
    //         case "reset":
    //             return {count: 0};
    //         default:
    //             break;
    //     }
    // }

    const [state, dispatch] = useReducer(reducerLogic, {count: 0});

    const incrActionObj = {type: "increment", value: 3};

    const decrActionObj = {type: "decrement", value: 2};

    const resetActionObj = {type: "reset"};

    return (
        <div>
            <h1>Use Reducer</h1>
            &nbsp;&nbsp;
            {state?.count}
            <br />
            <br />
            &nbsp;&nbsp;<button onClick={()=>dispatch(incrActionObj)}>+</button>&nbsp;
            &nbsp;&nbsp;<button onClick={()=>dispatch(decrActionObj)}>-</button>&nbsp;
            &nbsp;&nbsp;<button onClick={()=>dispatch(resetActionObj)}>Reset</button>                        
        </div>
    );
}

export default UseReducerCompEx1;