import { useReducer } from "react";

const UseReducerCompEx3 = () => {

    function actionBased(state:any, action:any) {
        switch(action.type) {
            case "A":
                console.log("Before change......")
                console.log(state);
                console.log("Inside the A");
                state.fName="zebra";
                console.log("After change......")
                console.log(state);
                console.log("--------------------------")
                //break;
                return state;
            case "B":
                console.log("Before change......")
                console.log(state);
                console.log("Inside the B");
                state.fName="fox";
                console.log("After change......")
                console.log(state);
                console.log("--------------------------")
                //break;
                return state;
            default:
                console.log("Inside the default");
                return {fName: "monkey"};
        }
    }

    const empObj = {
        fName: "Tiger",
        lName: "Lion",
        age: "10"
    }
    
    //const [state, dispatch] = useReducer(actionBased, {name:"val"});
    const [state, dispatch] = useReducer(actionBased, empObj);
    
    const aObj = {
        type: "A",
        name: "TigerRaja"
    }
    
    const bObj = {
        type: "B",
        name: "LionRaja"
    }
    
    return (
        <div>
            {state?.fName}
            <br />
            <button onClick={()=>dispatch(bObj)}>One</button>
            <button onClick={()=>dispatch(aObj)}>Two</button>
            <button onClick={()=>dispatch({type:"x"})}>Three</button>
        </div>
    );
}

export default UseReducerCompEx3;