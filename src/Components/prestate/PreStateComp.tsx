import { useState } from "react";

const PreStateComp = () => {

    const [val, setVal] = useState(0);

    const printVal = () => {
        console.log("--------val----------->", val);
        //setTimeout(() => setVal((val)=>val+1); printVal()}, 2000);
        //setTimeout(() => {setVal(val+1); printVal()}, 2000);
        setInterval(() => setVal((val)=>val+1), 2000);
        //setInterval(() => setVal(val+1), 2000);
    }

    return (
        <div>
            <button onClick={printVal}>Increment</button>
            <label><b>The Value:</b>{val}</label>
        </div>
    );
}

export default PreStateComp;