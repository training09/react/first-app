// function FirstComponent() {

import { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { FullNameContext } from "../App";

//     return (
//         <div>
//             <p>My First Component is Ready.........Traditional Based...........</p>
//         </div>
//     );
// }

// export const FullNameContext = createContext({});

const FirstComponent = () => {

    const location = useLocation();

    const navigate = useNavigate();

    //const firstName = "Tiger";
    const [firstName, setFirstName] = useState('');
    //const lastName = "Lion";
    const [lastName, setLastName] = useState('');

    useEffect(() => {
        console.log("Inside the useEffect......")
    }, []);

    const changeFirstName = (e: any) => {
        setFirstName(e.target.value);
    }

    const changeLastName = (e: any) => {
        setLastName(e.target.value);
    }

    const navToSecComp = () => {
        navigate(`/second/${firstName}/${lastName}`);
    }

    return (
        // <FullNameContext.Provider value={{fName: firstName, lName:lastName}}>
           <div>
                <p>My First Component is Ready....{location.state.userName}</p>
                <p>My First Component is Ready....{location.state.age}</p>
                <p>My First Component is Ready....{location.state.email}</p>
                <label><b>FirstName:</b></label>
                {/* <label>{firstName}</label> */}
                <input onChange={changeFirstName} />
                <br />
                <label><b>LastName:</b></label>
                <input onChange={changeLastName} />
                {/* <label>{lastName}</label> */}
                <br />
                <br />
                <label><b>Full Name:</b></label>
                <label>{firstName} {lastName}</label>
                <br />
                <br />
                <button onClick={navToSecComp}>Submit</button>
            </div>
        // </FullNameContext.Provider>
    );
}

export default FirstComponent;