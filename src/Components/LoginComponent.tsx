import { useState } from "react";
import { useNavigate } from "react-router-dom";

const LoginComponent = () => {

    const navigate = useNavigate();

    const [name, setName] = useState("");

    const [pwd, setPwd] = useState("");

    const changeUserName = (e:any) => {
        setName(e.target.value);
    }

    const changePwd = (e:any) => {
        setPwd(e.target.value);
    }

    const navToDashboard = () => {
        localStorage.setItem("authToken", "Auth - Local token");
        sessionStorage.setItem("sessToken", "Session token");
        console.log("Local storage..........");
        navigate("/default");
    }

    return (
        <div>
            <label>UserName:</label>
            <input type="text" value={name} onChange={changeUserName} />
            <br />
            <label>Password:</label>
            <input type="text" value={pwd} onChange={changePwd} />
            <br />
            <br />            
            <button onClick={navToDashboard}>Submit</button>
        </div>
    );
}

export default LoginComponent;