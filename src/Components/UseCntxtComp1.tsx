import React from 'react'
import UseCntxtComp2 from './UseCntxtComp2'
import { useNavigate } from 'react-router-dom';

const UseCntxtComp1 = () => {

    const navigate = useNavigate();

    const nameVal = "Tiger";

    const ageVal = 10;

    const perform = () => {
        console.log("Navigate to Comp3......");
        navigate("/comp3");
    }

    return (
        <div>
            UseCntxtComp1.........
            <UseCntxtComp2 name={nameVal} age={ageVal} performFn={perform}/>
        </div>
    )
}

export default UseCntxtComp1