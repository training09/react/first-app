import React, { useContext } from 'react'
import { FullNameContext } from '../App';

const UseCntxtComp2 = (props:any) => {

    const fullNameContext:any = useContext(FullNameContext);

    return (
        <div>
            UseCntxtComp2..........<br/>
            <label>Name:</label><h1>{props.name}</h1><br/>
            <label>Age:</label><h1>{props.age}</h1><br/>
            <label>Full Name: {fullNameContext.fName} {fullNameContext.lName}</label>
            <button onClick={props.performFn}>Submit</button>
        </div>
    )
}

export default UseCntxtComp2