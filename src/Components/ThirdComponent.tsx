import React from 'react'
import { useNavigate, useParams } from 'react-router-dom';

const ThirdComponent = () => {

  const params = useParams();

  const navigate = useNavigate();

  const fName = params.fName;

  const lName = params.lName;

  const nextComp = () => {
    navigate(`/fourth`);
  }

  return (
    <div>
      <p>ThirdComponent...........</p><br />
      <h2>First Name:{params.fName}</h2><br />
      <h2>Last Name:{params.lName}</h2><br />
      <button onClick={nextComp}>Next Comp</button><br />
    </div>
  )
}

export default ThirdComponent;