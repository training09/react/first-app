
import { useNavigate } from "react-router-dom";

const DefaultComponent = () => {

    const navigate = useNavigate();

    const empOb = {
        userName: "tiger",
        age: 10,
        email: "tiger@gmail.com"
    }

    const sendData = () => {
        navigate("/first", { state: empOb });
    }

    console.log("navigate function reference----------", navigate);

    return (
        <div>
            <h2>Welcome to First App - Default Component</h2>
            <button onClick={sendData}>Load First Comp</button>
        </div>
    );
}

export default DefaultComponent;