import axios from "axios";

axios.interceptors.request.use (
    (reqObj:any) => {
        console.log("Inside the request Interceptor");
        var authToken = localStorage.getItem("authToken");
        var sessionToken = sessionStorage.getItem("sessToken");
        reqObj.headers = {
            'authorization': 'Bearer :'+authToken+":"+sessionToken,
            'content-type': 'application/json',
            'monkey': 'donkey'
        }
        console.log("reqObj headers ----------->", reqObj);
        return reqObj;
    },
    (errObj:any)=> {
        return Promise.reject(errObj);
    }
);