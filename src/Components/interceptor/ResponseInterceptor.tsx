import axios from "axios";

axios.interceptors.response.use (
    (resObj) => {
        console.log("Inside the response interceptor....");
        resObj.headers = {
            'animal': 'tiger'
        }
        console.log("resObj headers ----------->", resObj);
        return resObj;
    },
    (errObj:any) => {
        return Promise.reject(errObj);
    }
);