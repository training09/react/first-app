//import React from 'react';
//import logo from './logo.svg';
//import './App.css';
import { createContext } from 'react';
import './App.css';
import RouterComponent from './Components/RouterComponent';
import UseRefComp from './Components/UseRefComp';
import UseReducerCompEx1 from './Components/ReducerComp/UseReducerCompEx1';
import UseReducerCompEx2 from './Components/ReducerComp/UseReducerCompEx2';
import ApiComponent from './Components/api/ApiComponent';
import UseReducerCompEx3 from './Components/ReducerComp/UseReducerCompEx3';

export const FullNameContext = createContext({});

function App() {
  return (
    <FullNameContext.Provider value={{ fName: "Tiger", lName: "Raja" }}>
      <RouterComponent />
      {/* <UseRefComp /> */}
      {/* <UseReducerCompEx2 /> */}
      {/* <UseReducerCompEx3 /> */}
      {/* <ApiComponent /> */}
    </FullNameContext.Provider>
  );
}

export default App;
